let init = function( options, callback ){
    // initialize the module
    // when this is asynchronous call the callback
    // with name of module as parameter and return
    // the name as follows:
    asynchronousCall().then(() => {
        callback("template");
    });
    return "template";

    // when the initialization is synchronouos then
    // return nothing!
};

// Add function etc. for your module

let test = function() {
    return "successful test of the template module";
}

let info = function() {
    return {
        version: '1',
        name: 'template',
        type: 'No type added.',
        note: 'This is a template of a module to create a new module.'
    }
};

// injecting into MTLG and expoxe public methods
MTLG.template = {
    test: test
};
MTLG.addModule(init, info);
