# Template Module

This module is a boilerplate for creating new modules.

## Usage

`git clone https://gitlab.com/mtlg-framework/mtlg-moduls/mtlg-module-template.git`

After cloning the repository you have to do some changes:

- rename the folder "mtlg-module-template" to "mtlg-module-MODULENAME" where MODULENAME is the name of your module 
- go into that folder
- rename "mtlg-module-template.js" to "mtlg-module-MODULENAME.js" where MODULENAME is the name of your module
- in package.json change `name`, `description` and `main`
- customize the README.md
- in demo/package.json change `"mtlg-module-template": "file:../../mtlg-module-template"` to `"mtlg-module-MODULENAME": "file:../../mtlg-module-MODULENAME"` where MODULENAME is the name of your module 
- in demo/dev/js/game.js line 10 change `require('mtlg-module-template');` to `require('mtlg-module-MODULENAME');`
- in mtlg-module-MODULENAME.js change `MTLG.template = ` to `MTLG.MODULENAME = ` where MODULENAME is the name of your module

To use your repository and not push into this one, use the following command 

`git remote set-url origin NEWURL` where NEWURL is the url to you repository like "https://gitlab.com/mtlg-framework/mtlg-moduls/mtlg-module-xapitracker.git"

Know you can customize the mtlg-module-MODULENAME.js file like you want.
In the demo folder is a game, where you should show how to use your module so that other developers can use it easily.